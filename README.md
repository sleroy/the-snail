> - [buy (mac or pc)](https://www.ircamlab.com/products/p2242-The-Snail/)
> - [buy (iPhone or iPad)](https://apps.apple.com/us/app/the-snail/id1189140204)
> - [Technical Support](https://help.ircamlab.com)

![IRCAM Lab](https://forum.ircam.fr/media/uploads/Softwares/The%20Snail/logo_ircam_lab.png)

## SEE WHAT YOU HEAR ##

For musicians, sound designers and composers

- Tune your instrument with ultimate precision
- Pinpoint harmonics, overtones & partials
- Analyze music with unmatched accuracy
- Catch intonation issues of audio tracks

## What is the Snail? ##
The Snail is a high-precision frequency-domain analyzer that delivers an intuitive representation of sound in real-time. It offers new ways of tuning music instruments in an extremely accurate fashion, displaying the frequencies (or partials) that shape the identity of an instrument. It provides with a unique visualization of sound and music, and can help anyone improve their aural skills and intonation!

At the heart of The Snail is a patented sound analysis technology, designed at the Laboratory of Science and Technology of Music and Sound, IRCAM-CNRS-UPMC, Paris.

## How does it work? ##
Think of a film camera turning at 24 frames per second filming a bicycle wheel turning at 24 revolutions a second, the spokes will appear to be standing still. That means, for a Fourier analysis of frequency at 439Hz compared to a tuning reference of 440Hz, the demodulated phase will turn at 1Hz (as a slightly desynchronised stroboscope makes a rotating object have a slow movement). Think of audio signals ’beating’ as there is a interference between two sounds at slightly different frequencies.

Compared to standard spectrum analyzers based on Fourier-type analysis, the frequency accuracy is enhanced by a process based on the demodulated phase.

The Snail makes the most of this process to:

1. Improve the frequency analysis precision at an adjustable rate, letting you zoom in on specific octaves and note ranges to improve your view in the display.
2. Go beyond stroboscopic techniques used in some high-quality tuners by analyzing all the components harmonics and inharmonicity, and rendering this information in a convenient visual form. You will see all the surrounding frequencies and overtones as well as the basic fundamentals.
3. Pinpoint tuning issues. The Snail shows all the slightly or highly inharmonic sounds and allows you to visualize the harmonicity in a song or an instrument; Eg. a “bad resonance” can be immediately identified, a resonance or note that is ‘out of tune’ in your instrument from poor intonation or resonances that cause some notes/frequencies to be more present (“ringing”) than others (which might be desirable or not...)
4. Reveal the details of a piece of music.  As a perfect companion to your own ear skills, The Snail can reveal visually the notes or chords used in a musical composition. If you have a solo track you can identify the notes used by an instrument that will reveal the progression in a ‘riff’ or solo and how it was played.

[![The Snail](https://forum.ircam.fr/media/uploads/the_snail_screenshot.png)](https://www.youtube.com/watch?v=T44UEma87vM)

## Product Features ##
- Simple intuitive interface
- Ultimate accuracy in tuning
- Different combinations of views available
- Audio player with waveform display to allow analysis of any song or sound (desktop only)
- Live input for tuning of external sources
- Tuner mode or Music mode
- Adjustable Tuning Reference pitch
- Transposing feature
- Display of phase relationships possible
- Hz information display
- User adjustable colour schemes

## System Requirements ##
- macOS: Intel or Arm processors, minimum 2Ghz, 2GB RAM, Mac OS 10.9 or higher, 64bit.
- Windows: Intel processors only, 2GB RAM, Windows 7 and higher, 64 bit.
- iOS: iPhone, iPad or iPod, iOS 9.3 and higher.
- Formats: Standalone App, Audio Unit (Mac), VST, AAX

> - [Sound Systems and Signals: Audio/Acoustics, InstruMents](https://www.ircam.fr/recherche/equipes-recherche/systemes-et-signaux-sonores-audioacoustique-instruments-s3am/) Team
> - Patented [CNRS](http://www.cnrs.fr) technology. 
> Patent FR 1455456 (May 2014).
> - Partnership: [CNRS](http://www.cnrs.fr),[French Ministry of Culture and Communication](French Ministry of Culture and Communication) and [Sorbonne Université](https://www.sorbonne-universite.fr/)

> - The desktop [demo version](http://www.ircamlab.com/products/p2242-The-Snail/) of The Snail is fully functional for 15 days. Download directly from the bottom of the Product page and install the full IRCAM Lab "The Snail" software. 

> - [Concert with the Snail & the Collegium Musicae](https://www.ircam.fr/medias/video/concert-snail/detail/)
> 28 mars 2017 ; Réalisation : Jeff Joly - PopMyFilm 
> - [NAMM 2017 by ASK.AUDIO](https://www.youtube.com/watch?v=KSWQWYMZiUw)
> - [NAMM 2017 by KR home-studio](https://www.youtube.com/watch?v=TnuXxFECqJw)
